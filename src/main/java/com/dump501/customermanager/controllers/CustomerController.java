package com.dump501.customermanager.controllers;

import com.dump501.customermanager.dto.CustomerDto;
import com.dump501.customermanager.services.CustomerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Objects;

@Controller
public class CustomerController {

    private final String loginRedirectionString = "redirect:/";
    @Autowired
    CustomerService customerService;

    @GetMapping("/customer")
    public String index(@RequestParam String id, Model model)  {
        model.addAttribute("id", id);
        try{
            System.out.println(id);

            ResponseEntity<String> responseEntity = customerService.getCustomers(id);
            if (responseEntity != null){
                if (responseEntity.getStatusCode().isSameCodeAs(HttpStatus.OK)){
                    String response = responseEntity.getBody();
                    System.out.println("response ========> "+ response);
                    ObjectMapper mapper = new ObjectMapper();
                    CustomerDto[] customers = mapper.readValue(response, CustomerDto[].class);
                    System.out.println("response ========> "+ customers[0].getEmail());
                    model.addAttribute("customers", customers);
                    return "customer/index";
                }
                return loginRedirectionString;
            }
            return "customer/index";
        } catch (Exception e){
            model.addAttribute("error", "Something went wrong please try later ...");
            return loginRedirectionString;
        }
    }

    @GetMapping("/customer/create")
    public ModelAndView create(@RequestParam String id, @RequestParam String uuid, Model model){
        ModelAndView mav = new ModelAndView("customer/create");
        model.addAttribute("id", id);
        if(!Objects.equals(uuid, "null")){
            try {
                // loop trought customers list and get custumer infos
                ResponseEntity<String> customersResponse = customerService.getCustomers(id);

                if(customersResponse != null && (customersResponse.getStatusCode().isSameCodeAs(HttpStatus.OK))){
                        String jsonString = customersResponse.getBody();
                        ObjectMapper mapper = new ObjectMapper();
                        CustomerDto[] customers = mapper.readValue(jsonString, CustomerDto[].class);
                        for (CustomerDto customer : customers) {
                            if (Objects.equals(customer.getUuid(), uuid)) {
                                mav.addObject("customer", customer);
                                mav.addObject("action", "/update");
                                mav.addObject("uuid", uuid);
                                return mav;
                            }
                        }


                }
            } catch (Exception e) {
                System.out.println("Exception ====================> "+e.getMessage());
            }
        }
        mav.addObject("customer", new CustomerDto());
        mav.addObject("action", "");
        return mav;
    }

    @PostMapping("/customer")
    public String store(@RequestParam String id, @ModelAttribute CustomerDto customerDto, Model model){
        model.addAttribute("id", id);
        try {
            System.out.println(customerDto.getEmail());

            //make api call
            ResponseEntity<String> response = customerService.storeCustomer(id, customerDto);
            if(response != null){
                if(response.getStatusCode().isSameCodeAs(HttpStatus.CREATED)){
                    return "redirect:/customer?id="+id;
                }
                model.addAttribute("error", "Something went wrong in our end. Please try later ....");
                return loginRedirectionString;
            }
        } catch (Exception e){
            System.out.println("Exception ===================>  " + e.getMessage());
            model.addAttribute("error", "Somthing went wrong on our side please try later");
            return "500";
        }

        return loginRedirectionString;
    }


    @PostMapping("/customer/update")
    public String update(@RequestParam String id, @RequestParam String uuid, @ModelAttribute CustomerDto customerDto, Model model){
        model.addAttribute("id", id);

        try {
            //validate user

            // loop trought customers list and get custumer infos
            ResponseEntity<String> customersResponse = customerService.getCustomers(id);

            if(customersResponse != null){
                if(customersResponse.getStatusCode().isSameCodeAs(HttpStatus.OK)){
                    String jsonString = customersResponse.getBody();
                    ObjectMapper mapper = new ObjectMapper();
                    CustomerDto[] customers = mapper.readValue(jsonString, CustomerDto[].class);
                    for (CustomerDto customer : customers) {
                        if (Objects.equals(customer.getUuid(), uuid)) {
                            //update customer
                            ResponseEntity<String> updateResponse = customerService.updateCustomer(id, customerDto);
                            System.out.println("Name =================> "+customerDto.getFirst_name());

                            if (updateResponse != null && (updateResponse.getStatusCode().isSameCodeAs(HttpStatus.OK))){
                                    System.out.println("Body =================> "+updateResponse.getBody());
                                    model.addAttribute("id", id);
                                    model.addAttribute("message", "Updated successfully the user "+ customerDto.getFirst_name());
                                    return "redirect:/customer?id="+id+"&uuid=null";

                            }
                        }
                    }

                }

                return loginRedirectionString;
            }
            // redirect to view

        } catch (Exception exception){
            System.out.println("exception : =================> " + exception.getMessage());
            model.addAttribute("error", "Somthing went wrong on our side please try later");
            return "500";
        }

        return null;
    }

    @GetMapping("/customer/delete")
    public String delete(@RequestParam String id, @RequestParam String uuid, Model model){
        model.addAttribute("id", id);
        try{
            ResponseEntity<String> customersResponse = customerService.deleteCustomer(id, uuid);
            if (customersResponse != null && (customersResponse.getStatusCode().isSameCodeAs(HttpStatus.OK))){
                    return "redirect:/customer?id="+id+"&uuid=null";
            }
            model.addAttribute("error", "Unable to delete the customer");
            return "500";
        } catch (Exception e){
            System.out.println("exception : =================> " + e.getMessage());
            model.addAttribute("error", "Somthing went wrong on our side please try later");
            return "500";
        }
    }
}
