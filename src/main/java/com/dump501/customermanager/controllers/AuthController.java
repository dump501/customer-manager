package com.dump501.customermanager.controllers;

import com.dump501.customermanager.dto.LoginDto;
import com.dump501.customermanager.dto.LoginResponseDto;
import com.dump501.customermanager.services.AuthService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

import static com.dump501.customermanager.helpers.ApiHelper.loginUrl;

@Controller
public class AuthController {

    @Autowired
    AuthService authService;
    @GetMapping("/")
    public String showLoginForm(Model model){
        model.addAttribute("user", new LoginDto());
        return "login";
    }

    @PostMapping("/login")
    public String login(@ModelAttribute LoginDto loginDto, Model model) throws JsonProcessingException {
        System.out.println(loginDto.getLogin_id());

        ResponseEntity<String> response = authService.login(loginDto);
        if(response != null && response.getStatusCode().isSameCodeAs(HttpStatus.OK)){
            ObjectMapper mapper = new ObjectMapper();
            LoginResponseDto map = mapper.readValue(response.getBody(), LoginResponseDto.class);
            String token = map.getAccess_token();
            return "redirect:/customer?id="+token;
        }

        return "redirect:/";
    }
}
