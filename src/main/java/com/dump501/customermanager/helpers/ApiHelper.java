package com.dump501.customermanager.helpers;

public class ApiHelper {
    private ApiHelper(){}
    public static final String loginUrl = "https://qa2.sunbasedata.com/sunbase/portal/api/assignment_auth.jsp";

    public static final String customerList = "https://qa2.sunbasedata.com/sunbase/portal/api/assignment.jsp?cmd=get_customer_list";

    public static final String customerCreate="https://qa2.sunbasedata.com/sunbase/portal/api/assignment.jsp?cmd=create";

    public static  final String customerUpdate="https://qa2.sunbasedata.com/sunbase/portal/api/assignment.jsp?cmd=update";

    public static final String customerDelete="https://qa2.sunbasedata.com/sunbase/portal/api/assignment.jsp?cmd=delete";
}
