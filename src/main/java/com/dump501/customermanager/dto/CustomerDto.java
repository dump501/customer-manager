package com.dump501.customermanager.dto;

import com.dump501.customermanager.models.Customer;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class CustomerDto {
    private String uuid;
    private String first_name;
    private String last_name;
    private String street;
    private String address;
    private String city;
    private String state;
    private String email;
    private String phone;


    public Customer toUser(){
        return new Customer(uuid, first_name, last_name, street, address, city, state, email, phone);
    }
}
