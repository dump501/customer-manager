package com.dump501.customermanager.services;

import com.dump501.customermanager.dto.CustomerDto;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import static com.dump501.customermanager.helpers.ApiHelper.*;

@Service
public class CustomerService {

    public ResponseEntity<String> getCustomers(String token){
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", "Bearer "+token);

            HttpEntity<String> request = new HttpEntity<>("", headers);

            RestTemplate restTemplate = new RestTemplate();
            return restTemplate.exchange(customerList, HttpMethod.GET, request, String.class);
        } catch (Exception exception){
            System.out.println("Exception =================> "+exception.getMessage());
            return null;
        }
    }

    public ResponseEntity<String> storeCustomer(String token, CustomerDto customerDto){
        try{
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", "Bearer "+token);

            //create user
            HttpEntity<CustomerDto> request = new HttpEntity<>(
                    customerDto, headers
            );

            //make api call
            RestTemplate restTemplate = new RestTemplate();
            return restTemplate.exchange(customerCreate, HttpMethod.POST, request, String.class);
        } catch (Exception exception){
            System.out.println("Exception =================> "+exception.getMessage());
            return null;
        }
    }

    public ResponseEntity<String> updateCustomer(String token, CustomerDto customerDto){
        System.out.println("Custumer dto =================> "+customerDto.getFirst_name());
        try{
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", "Bearer "+token);

            HttpEntity<CustomerDto> request = new HttpEntity<>(
                    customerDto, headers
            );

            //make api call
            RestTemplate restTemplate = new RestTemplate();
            return restTemplate.exchange(customerUpdate+"&uuid="+customerDto.getUuid(), HttpMethod.POST, request, String.class);
        } catch (Exception exception){
            System.out.println("Exception =================> "+exception.getMessage());
            return null;
        }
    }

    public ResponseEntity<String> deleteCustomer(String token, String uuid){
        try{
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", "Bearer "+token);

            HttpEntity<String> request = new HttpEntity<>(
                    "", headers
            );

            //make api call
            RestTemplate restTemplate = new RestTemplate();
            return restTemplate.exchange(customerDelete+"&uuid="+uuid, HttpMethod.POST, request, String.class);
        } catch (Exception exception){
            System.out.println("Exception =================> "+exception.getMessage());
            return null;
        }
    }
}
