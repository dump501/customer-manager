package com.dump501.customermanager.services;

import com.dump501.customermanager.dto.LoginDto;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import static com.dump501.customermanager.helpers.ApiHelper.loginUrl;

@Service
public class AuthService {
    public ResponseEntity<String> login(LoginDto loginDto){
        try {


            HttpEntity<LoginDto> request = new HttpEntity<>(
                    loginDto
            );

            //make api call
            RestTemplate restTemplate = new RestTemplate();
            return restTemplate.exchange(loginUrl, HttpMethod.POST, request, String.class);
        } catch (Exception e){
            System.out.println(e.getMessage());
            return null;
        }
    }
}
