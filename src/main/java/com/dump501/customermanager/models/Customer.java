package com.dump501.customermanager.models;

import com.dump501.customermanager.dto.CustomerDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class Customer {
    private String uuid;
    private String firstName;
    private String lastName;
    private String street;
    private String address;
    private String city;
    private String state;
    private String email;
    private String phone;

    public CustomerDto toDto(){
        return new CustomerDto(uuid, firstName, lastName, street, address, city, state, email, phone);
    }
}
