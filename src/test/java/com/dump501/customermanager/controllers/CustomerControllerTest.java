package com.dump501.customermanager.controllers;

import com.dump501.customermanager.dto.CustomerDto;
import com.dump501.customermanager.helpers.TestHelper;
import com.dump501.customermanager.services.CustomerService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CustomerController.class)
public class CustomerControllerTest {


    @Autowired
    private MockMvc mockMvc;

    @Mock
    private RestTemplate restTemplate;

    @MockBean
    private CustomerService customerService;

    @Test
    void should_returnCustomersListPage_when_naviguateToCustomersUrl() throws Exception{
        CustomerDto[] customersList = TestHelper.buildCustomersList();
        ObjectMapper mapper = new ObjectMapper();

        when(customerService.getCustomers(TestHelper.token))
                .thenReturn(new ResponseEntity<String>(mapper.writeValueAsString(customersList), HttpStatus.OK));

        mockMvc
                .perform(get("/customer?id="+TestHelper.token))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("customers"))
                .andExpect(model().attributeExists("id"));
    }


    @Test
    void should_returnCustomerFormRegistration_when_naviguateToCustomerCreationUrl() throws Exception{
        mockMvc
                .perform(get("/customer/create?id="+TestHelper.token+"&uuid=null"))
                .andExpect(status().isOk())
                .andExpect(model().attributeExists("customer"))
                .andExpect(model().attributeExists("id"));
    }


    @Test
    void should_returnRedirect_when_postRequestToCustomerCreationUrl() throws Exception{
        CustomerDto[] customersList = TestHelper.buildCustomersList();
        ObjectMapper mapper = new ObjectMapper();

        when(customerService.storeCustomer(TestHelper.token, customersList[0]))
                .thenReturn(new ResponseEntity<String>(mapper.writeValueAsString(customersList), HttpStatus.CREATED));

        mockMvc
                .perform(post("/customer?id="+TestHelper.token))
                .andExpect(status().isFound());
    }

    @Test
    void should_returnRedirect_when_postRequestToCustomerUpdateUrl() throws Exception{
        CustomerDto[] customersList = TestHelper.buildCustomersList();
        ObjectMapper mapper = new ObjectMapper();

        when(customerService.getCustomers(TestHelper.token))
                .thenReturn(new ResponseEntity<String>(mapper.writeValueAsString(customersList), HttpStatus.OK));


        when(customerService.updateCustomer(TestHelper.token, customersList[0]))
                .thenReturn(new ResponseEntity<String>(mapper.writeValueAsString(customersList[0]), HttpStatus.OK));

        mockMvc
                .perform(post("/customer/update?id="+TestHelper.token+"&uuid=test987786576787"))
                .andExpect(status().isFound());
    }


    @Test
    void should_returnRedirect_when_postRequestToCustomerDeleteUrl() throws Exception{

        when(customerService.deleteCustomer(TestHelper.token, "test987786576787"))
                .thenReturn(new ResponseEntity<String>("Deleted successsfully", HttpStatus.OK));

        mockMvc
                .perform(get("/customer/delete?id="+TestHelper.token+"&uuid=test987786576787"))
                .andExpect(status().isFound());
    }
}
