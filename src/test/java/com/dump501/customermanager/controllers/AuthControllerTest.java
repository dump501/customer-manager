package com.dump501.customermanager.controllers;

import com.dump501.customermanager.dto.LoginDto;
import com.dump501.customermanager.dto.LoginResponseDto;
import com.dump501.customermanager.helpers.TestHelper;
import com.dump501.customermanager.services.AuthService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(AuthController.class)
class AuthControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Mock
    private RestTemplate restTemplate;

    @MockBean
    private AuthService authService;

    @Test
    void should_returnLoginForm_when_NavigateToRootUrl() throws Exception {
        mockMvc
                .perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(view().name("login"))
                .andExpect(model().attributeExists("user"));
    }

    @Test
    void should_returnRedirect_when_PostRequestToLoginUrl() throws Exception {
        LoginResponseDto accessTokenResponse = TestHelper.buildLAccessTokenResponse();
        LoginDto loginDto = new LoginDto("test@sunbasedata.com", "Test@123");

        ObjectMapper mapper =new ObjectMapper();

        when(authService.login(loginDto))
                .thenReturn(new ResponseEntity<String>(mapper.writeValueAsString(accessTokenResponse), HttpStatus.OK));

        mockMvc
                .perform(post("/login"))
                .andExpect(status().isFound());
    }
}