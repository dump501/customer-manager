package com.dump501.customermanager.helpers;


import com.dump501.customermanager.dto.CustomerDto;
import com.dump501.customermanager.dto.LoginResponseDto;
import com.dump501.customermanager.models.Customer;

import java.util.ArrayList;
import java.util.List;

public class TestHelper {

    public static final String token = "dGVzdEBzdW5iYXNlZGF0YS5jb206VGVzdEAxMjM=";
    public static final String uuid = "test987786576787";
    public static LoginResponseDto buildLAccessTokenResponse(){
        return new LoginResponseDto(TestHelper.token);
    }
    
    public static CustomerDto[] buildCustomersList(){
        List<CustomerDto> customersList = new ArrayList<CustomerDto>();
        for (int i = 0; i < 3; i++) {
            CustomerDto customer = new CustomerDto();
            customer.setUuid(TestHelper.uuid+i);
            customer.setFirst_name("FirstName" + i);
            customer.setLast_name("Last Name" + i);
            customer.setEmail("email@test.com" + i);
            customer.setCity("city" + i);
            customer.setPhone("12134" + i);
            customer.setState("state" + i);
            customer.setAddress("address" + i);
            customer.setStreet("street" + i);
            if (i == 1){
                customer.setUuid(TestHelper.uuid);
            }
            
            customersList.add(customer);
        }
        CustomerDto[] customers = new CustomerDto[customersList.size()];
        customers = customersList.toArray(customers);
        return customers;
    }
    
}
