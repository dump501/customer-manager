## Custumer manager
### A customer manager application build as assignment

This project is developerd using Spring boot V3

### installation
#### From old school ? use the submited zip file (jar file execution)
* open the zipped folder
* cd in the cloned folder `cd custom-manager`
* run the command `java -jar target/customer-manager-0.0.1-SNAPSHOT.jar` to run the jar 
* or by using maven by running the command `mvn spring-boot:run` to run the development server
* open a web browser and navigate to http://localhost:8080


#### From new school ? use git (clone and start development server)
* Clone the project using git run `git clone https://gitlab.com/dump501/customer-manager`
* cd in the cloned folder `cd custom-manager`
* run the command `mvn spring-boot:run` to launch the development server using maven
* open a web browser and navigate to http://localhost:8080

### Tools used
* thymeleaf as template engine
* spring boot starter test for testing
* mockito for controllers unit tests
* gitlab ci for building

### Todo next 
* integration tests
* encrease test coverage pecentage
* contenerization of the app (using docker, k8s)
* add test in gitlab CI and make a real CI/CD pipeline
* manage git branches

made with ❤️ by Fritz <tsafack07albin@gmail.com> for SunbaseData intership application.
